<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KtpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ktp = DB::table('ktp')->get();

        return view('ktp.index', compact('ktp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ktp.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nik' => 'required',
            'nama' => 'required',
            'tempatLahir' => 'required',
            'tanggalLahir' => 'required',
            'alamat' => 'required',
            'agama' => 'required',
            'status' => 'required',
            'pekerjaan' => 'required',
            'kewarganegaraan' => 'required',
        ]);

        DB::table('ktp')->insert([
            'nik' => $request['nik'],
            'nama' => $request['nama'],
            'tempatLahir' => $request['tempatLahir'],
            'tanggalLahir' => $request['tanggalLahir'],
            'alamat' => $request['alamat'],
            'agama' => $request['agama'],
            'status' => $request['status'],
            'pekerjaan' => $request['pekerjaan'],
            'kewarganegaraan' => $request['kewarganegaraan'],
        ]);

        return redirect('/ktp');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ktp = DB::table('ktp')->where('id', $id)->first();
        return view('ktp.show', compact('ktp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ktp = DB::table('ktp')->where('id', $id)->first();
        return view('ktp.edit', compact('ktp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nik' => 'required',
            'nama' => 'required',
            'tempatLahir' => 'required',
            'tanggalLahir' => 'required',
            'alamat' => 'required',
            'agama' => 'required',
            'status' => 'required',
            'pekerjaan' => 'required',
            'kewarganegaraan' => 'required',
        ]);

        $query = DB::table('ktp')
            ->where('id', $id)
            ->update([
                'nik' => $request['nik'],
                'nama' => $request['nama'],
                'tempatLahir' => $request['tempatLahir'],
                'tanggalLahir' => $request['tanggalLahir'],
                'alamat' => $request['alamat'],
                'agama' => $request['agama'],
                'status' => $request['status'],
                'pekerjaan' => $request['pekerjaan'],
                'kewarganegaraan' => $request['kewarganegaraan'],
            ]);

        return redirect('/ktp');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('ktp')->where('id', $id)->delete();

        return redirect('/ktp');
    }
}
