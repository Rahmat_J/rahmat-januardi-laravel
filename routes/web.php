<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/ktp/create', 'KtpController@create');
Route::post('/ktp', 'KtpController@store');
Route::get('/ktp', 'ktpController@index');
Route::get('/ktp/{ktp_id}', 'KtpController@show');
Route::get('/ktp/{ktp_id}/edit', 'KtpController@edit');
Route::put('/ktp/{ktp_id}', 'KtpController@update');
Route::delete('/ktp/{ktp_id}', 'KtpController@destroy');