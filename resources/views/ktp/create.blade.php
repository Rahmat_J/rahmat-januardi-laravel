<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="/ktp" method="POST">
        @csrf
        <div class="form-group">
          <label>nik</label>
          <input type="number" name="nik" class="form-control">
        </div>
        <div class="form-group">
          <label>Nama</label>
          <input type="text" name="nama" class="form-control">
        </div>
        <div class="form-group">
          <label>tempat Lahir</label>
          <input type="text" name="tempatLahir" class="form-control">
        </div>
        <div class="form-group">
          <label>tanggal Lahir</label>
          <input type="date" name="tanggalLahir" class="form-control">
        </div>
        <div class="form-group">
          <label>alamat</label>
          <textarea name="alamat" class="form-control" id="" cols="30" rows="10"></textarea>
        </div>
        <div class="form-group">
          <label>agama</label>
          <input type="text" name="agama" class="form-control">
        </div>
        <div class="form-group">
          <label>status</label>
          <input type="text" name="status" class="form-control">
        </div>
        <div class="form-group">
          <label>pekerjaan</label>
          <input type="text" name="pekerjaan" class="form-control">
        </div>
        <div class="form-group">
          <label>kewarganegaraan</label>
          <input type="text" name="kewarganegaraan" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
</body>
</html>