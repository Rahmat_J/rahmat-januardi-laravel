<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="/ktp/{{$ktp->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>nik</label>
            <input type="number" name="nik" class="form-control" value="{{$ktp->nik}}">
          </div>
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control" value="{{$ktp->nama}}">
          </div>
          <div class="form-group">
            <label>tempat Lahir</label>
            <input type="text" name="tempatLahir" class="form-control" value="{{$ktp->tempatLahir}}">
          </div>
          <div class="form-group">
            <label>tanggal Lahir</label>
            <input type="date" name="tanggalLahir" class="form-control" value="{{$ktp->tanggalLahir}}">
          </div>
          <div class="form-group">
            <label>alamat</label>
            <textarea name="alamat" class="form-control" id="" cols="30" rows="10">{{$ktp->alamat}}</textarea>
          </div>
          <div class="form-group">
            <label>agama</label>
            <input type="text" name="agama" class="form-control" value="{{$ktp->agama}}">
          </div>
          <div class="form-group">
            <label>status</label>
            <input type="text" name="status" class="form-control" value="{{$ktp->status}}">
          </div>
          <div class="form-group">
            <label>pekerjaan</label>
            <input type="text" name="pekerjaan" class="form-control" value="{{$ktp->pekerjaan}}">
          </div>
          <div class="form-group">
            <label>kewarganegaraan</label>
            <input type="text" name="kewarganegaraan" class="form-control" value="{{$ktp->kewarganegaraan}}">
          </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
</body>
</html>