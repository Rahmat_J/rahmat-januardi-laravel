<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <table class="table">
        <tbody>
            @forelse ($ktp as $key => $item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item->nik }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->tempatLahir }}</td>
                    <td>{{ $item->tanggalLahir }}</td>
                    <td>{{ $item->alamat }}</td>
                    <td>{{ $item->agama }}</td>
                    <td>{{ $item->status }}</td>
                    <td>{{ $item->pekerjaan }}</td>
                    <td>{{ $item->kewarganegaraan }}</td>
                    <td>
                        <form action="/ktp/{{ $item->id }}" method="POST">
                            <a href="/ktp/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/ktp/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                            @method('delete')
                            @csrf
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data Masih Kosong</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</body>

</html>
